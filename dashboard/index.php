<!doctype html>
<html ng-app="hiringbooksAngular">
  <head>
    <meta charset="utf-8">
    <title>hiringbooksAngular</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <base href="/hiringbooks/dashboard/">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="styles/vendor.css">

    <link rel="stylesheet" href="styles/app.css">
  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <main ui-view></main>

    <script src="scripts/vendor.js"></script>

    <script src="scripts/app.js"></script>

  </body>
</html>
