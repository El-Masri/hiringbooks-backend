<?php

class books_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->model('notifications_model');
        $this->load->model('activity_model');
    }

    public function get_book($id)
    {
        if ($id != false) {
            $query = $this->db->get_where('books', array('id' => $id));
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function get_book_bygoodreadsid($id)
    {
        if ($id != false) {
            $query = $this->db->get_where('books', array('goodreads_id' => $id));
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function get_requests_bygoodreadsid($id)
    {
        if ($id != false) {
            $query = $this->db->get_where('requests', array('goodreads_id' => $id));

            return $query->row_array();
        } else {
            return false;
        }
    }

    public function get_allbooks()
    {
        $this->db->order_by('id', 'desc');
        $query = $this->db->get_where('books');

        return $query->result();
    }

    public function get_limited_books()
    {
        $this->db->order_by('id', 'desc');
        $query = $this->db->get_where('books', null, 4, 0);
        return $query->result();
    }

    public function get_books_byuserid($id)
    {
        if ($id != false) {
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('books', array('user_id' => $id));

            return $query->result();
        } else {
            return false;
        }
    }

    public function get_comments_byuserid($id)
    {
        if ($id != false) {
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('comments', array('topic_type' => 'book', 'target_user_id' => $id));

            return $query->result();
        } else {
            return false;
        }
    }

    public function post_comment($data)
    {
        if (!empty($data)) {
            $id = $data['topic_id'];
            $update_data = array('comments_count' => $data['comments_count']);
            $update_query = $this->db->update_string('books', $update_data, "id = $id");
            $this->db->query($update_query);
            unset($data['comments_count']);
            $notification_data = array(
              'name' => $data['fullname'],
              'notification' => 'commented on your book.',
              'user_id' => $data['user_id'],
              'target_user_id' => $data['target_user_id'],
              'seen' => false,
              'topic_id' => $data['topic_id'],
              'topic_type' => 'books',
              'timestamp' => $data['timestamp'],
            );
            $this->notifications_model->post_notification($notification_data);
            unset($data['fullname']);
            $create_query = $this->db->insert_string('comments', $data);
            $this->db->query($create_query);

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }

    public function post_book($data)
    {
        if (!empty($data)) {
            if ($this->get_requests_bygoodreadsid($data['goodreads_id'])) {
                $results = $this->get_requests_bygoodreadsid($data['goodreads_id']);
                foreach ($results as $b_request) {
                    $notification_data = array(
                      'timestamp' => $data['timestamp'],
                      'notification' => 'offered a book you requested.',
                      'user_id' => $data['user_id'],
                      'target_user_id' => $b_request->user_id,
                      'seen' => false,
                      'name' => $data['fullname'],
                      'topic_id' => $b_request->id,
                      'topic_type' => 'books',
                    );
                    $this->notifications_model->post_notification($notification_data);
                }
            }
            $activity_data = array(
             'timestamp' => $data['timestamp'],
             'user_name' => $data['fullname'],
             'activity' => "has posted a book.",
             'user_id' => $data['user_id'],
             'seen' => false,
             'topic_type' => 'books',
            );
            unset($data['fullname']);
            $query = $this->db->insert_string('books', $data);
            $this->db->query($query);
            $last_inserted_book = $this->books_model->get_book_bygoodreadsid($data['goodreads_id']);
            $activity_data['topic_id'] = $last_inserted_book['id'];
            $this->activity_model->set_activity($activity_data);
            return $this->db->affected_rows();
        } else {
            return false;
        }
    }

    public function delete_book($id)
    {
        if ($id != false) {
            $this->db->delete('books', array('id' => $id));

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }

    public function status_book($body)
    {
        $id = $body['id'];
        if ($id != false) {
            $data = array('status' => $body['status']);
            $query = $this->db->update_string('books', $data, "id = $id");
            $this->db->query($query);

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }

    public function price_book($body)
    {
        $id = $body['id'];
        if ($id != null) {
            $data = array('price' => $body['price']);
            $query = $this->db->update_string('books', $data, "id = $id");
            $this->db->query($query);

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }
    public function delete_user($id)
    {
        if ($id != false) {
            $this->db->delete('books', array('user_id' => $id));

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }
}
