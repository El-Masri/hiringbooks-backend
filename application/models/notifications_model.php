<?php

use ElephantIO\Client as Elephant;
use ElephantIO\Engine\SocketIO\Version1X as Version1X;

class notifications_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_byuserid($user_id)
    {
        if ($user_id != null) {
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('notifications', array('target_user_id' => $user_id), 5);

            return $query->result();
        } else {
            return false;
        }
    }

    public function set_seen($notification_id)
    {
        if (!empty($notification_id)) {
            $update_string = array('seen' => 1);
            $query = $this->db->update_string('notifications', $update_string, "id = $notification_id");
            $this->db->query($query);

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }

    public function send_socket($socket_data)
    {
        $client = new Elephant(new Version1X('http://localhost:7000'));
        $client->initialize();
        $client->emit('notification', ['notification' => $socket_data]);
        $client->close();
    }

    public function post_notification($notification_data)
    {
        if (!empty($notification_data)) {
            $this->send_socket(json_encode($notification_data));
            // $this->send_email($notification_data);
            $notification_query = $this->db->insert_string('notifications', $notification_data);
            $this->db->query($notification_query);

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }
    public function delete_user($id)
    {
        if ($id != false) {
            $this->db->delete('notifications', array('target_user_id' => $id));

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }
}
