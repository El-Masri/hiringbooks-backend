<?php

class reports_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->library('ion_auth');
    }

    public function get_reports($user_id, $target_id)
    {
        if ($user_id != null) {
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('reports', array('user_id' => $user_id, 'target_user_id' => $target_id));
            return $query->result();
        } else {
          $this->db->order_by('id', 'desc');
          $query = $this->db->get_where('reports');
          return $query->result();
        }
    }

    public function get_byuserid($user_id)
    {
        if ($user_id != null) {
            $query = $this->db->get_where('reports', array('target_user_id' => $user_id));

            return $query->result();
        } else {
            return false;
        }
    }

    public function set_report($report)
    {
        if (!empty($report)) {
            $query_report = $report;
            unset($query_report['reports_count']);
            $query = $this->db->insert_string('reports', $query_report);
            $this->db->query($query);
            $this->ion_auth_model->update($report['target_user_id'], array('reports_count' => $report['reports_count']));
            return true;
        } else {
            return false;
        }
    }

    public function set_seen()
    {
        $this->db->order_by('id', 'desc');
        $getReport = $this->db->get_where('reports');
        $result = $getReport->result();
        foreach ($result as $report) {
            $id = $report->id;
            $query = $this->db->update_string('reports', array('seen' => true), "id = $id");
            $this->db->query($query);
        }
        return true;
    }
}
