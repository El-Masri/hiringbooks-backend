<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class activity_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function get_activity($id) {
   if ($id != null) {
       $this->db->order_by("id", "desc");
       $query = $this->db->get_where('activity', array('user_id' => $id));
       return $query->result();
   } else {
       return false;
   }
  }

  public function set_activity($data) {
   if (!empty($data)) {
       $query = $this->db->insert_string('activity', $data);
       $this->db->query($query);
       return $this->db->affected_rows();
   } else {
       return false;
   }
  }
  public function delete_user($id)
  {
      if ($id != false) {
          $this->db->delete('activity', array('user_id' => $id));

          return $this->db->affected_rows();
      } else {
          return false;
      }
  }
}