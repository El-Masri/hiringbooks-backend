<?php

use ElephantIO\Client as Elephant;
use ElephantIO\Engine\SocketIO\Version1X as Version1X;

class ratings_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_byuserid($user_id)
    {
        if ($user_id != null) {
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('ratings', array('target_user_id' => $user_id));
            return $query->result();
        } else {
            return false;
        }
    }

    public function send_socket($socket_data)
    {
        $client = new Elephant(new Version1X('http://localhost:7000'));
        $client->initialize();
        $client->emit('notification', ['notification' => $socket_data]);
        $client->close();
    }

    public function update_user_ratings($id)
    {
     $userratings_query = $this->db->get_where('ratings', array("target_user_id" => $id));
     $ratings = $userratings_query->result();
     $rating_sum = 0;
     foreach ($ratings as $rating) {
      $rating_sum =+ $rating->rating;
     }
     $rating_sum = $rating_sum / count($ratings);
     $update_count = $this->db->update_string('users', array("rating"=>$rating_sum), "id = $id");
     $this->db->query($update_count);
     return $this->db->affected_rows();
    }

    public function post_rating($rating_data)
    {
        if (!empty($rating_data)) {
            $id = $rating_data['target_user_id'];
            $user_id = $rating_data['user_id'];
        // $this->send_socket(json_encode($rating_data));
        // $this->send_email($rating_data);
            $previous_query = $this->db->get_where('ratings', array('target_user_id' => $rating_data['target_user_id'], 'user_id' => $rating_data['user_id']));
            if (empty($previous_query->row_array())) {
                $update_data = array('ratings_count' => (int)$rating_data['ratings_count']+1);
                $update_count = $this->db->update_string('users', $update_data, "id = $id");
                $this->db->query($update_count);
                unset($rating_data['ratings_count']);
                $rating_query = $this->db->insert_string('ratings', $rating_data);
                $this->db->query($rating_query);
                $this->update_user_ratings($id);
                return 2;
            } else {
                $rating_query = $this->db->update_string('ratings', array('rating' => $rating_data['rating']), array('user_id' => $user_id, 'target_user_id' => $id));
                $this->db->query($rating_query);
                $this->update_user_ratings($id);
                return 1;
            }
        } else {
            return false;
        }
    }
}
