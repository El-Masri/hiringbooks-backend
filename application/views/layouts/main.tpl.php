<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $this->config->item('site_title'); if(isset($title))echo " ".$this->config->item('site_title_delimiter')." ".$title;?></title>
    <link type='text/css' href="<?php echo base_url();?>css/vendor.css" rel='Stylesheet' />
    <link type='text/css' href="<?php echo base_url();?>css/styles.css" rel='Stylesheet' />
    <link type='text/css' href="<?php echo base_url();?>css/main.css" rel='Stylesheet' />
    <!-- <script src="<?php echo base_url();?>js/jquery.js" type="text/javascript"></script>
    <link type="text/css' href='<?php echo base_url();?>css/ui-lightness/jquery-ui.css" rel='Stylesheet' /> -->
    <!-- <script src="<?php echo base_url();?>js/jquery-ui.js" type="text/javascript"></script> -->
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

    <script type='text/javascript'>
        <?php if (isset($js)){echo $js;}?>
    </script>

    <?php
    	if(isset($head) && is_array($head)) {
    		foreach ($head as $headObject) {
    			echo $headObject;
    		}
    	}
    ?>
</head>
<body <?php if(isset($onload)){echo "onload=$onload";}?> class="form-body">
	<div id="form-container">

    <?php echo $content; ?>

	</div>
</body>
</html>
