<div class='wrapper'>

	<h1>Sign in</h1>
	<img src="../images/logo.png" class="logo" alt="" />

	<h4 id="infoMessage"><?php echo $message;?></h4>

    <?php echo form_open("auth/login");?>

		<div class="form-group">

      	<?php echo form_input($identity, "","class='form-control' placeholder='Email/Username'");?>
      </div>

			<div class="form-group">

      	<?php echo form_input($password, "","class='form-control' placeholder='Password'");?>
      </div>

			<div class="checkbox">
	      <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?><label for="remember">keep me logged in</label>
	  </div>

	<a href="forgot_password">Forgot your <b>password?</b></a></p>

		<div class="form-group">
<?php echo form_submit('submit', 'Login', 'class="btn btn-default"');?>
</div>

		<!-- <div class="form-group facebook-btn">
			<i class="fa fa-facebook-square"></i>
			<a href="loginfacebook" class="btn btn-default btn-facebook">Login with facebook</a>
</div> -->


    <?php echo form_close();?>