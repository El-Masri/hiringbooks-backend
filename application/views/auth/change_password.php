<div class="wrapper">
		<h1>Change Password</h1>
		<img src="../images/logo.png" class="logo" alt="" />

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/change_password");?>

      <div class="form-group">
        <?php echo form_input($old_password, "", "class='form-control' placeholder='Old Password'");?>
      </div>

      <div class="form-group">
        <?php echo form_input($new_password, "", "class='form-control' placeholder='New Password'");?>
      </div>

      <div class="form-group">
        <?php echo form_input($new_password_confirm, "", "class='form-control' placeholder='Confirm New Password'");?>
      </div>

      <div class="form-group">
       <?php echo form_input($user_id, "", "class='form-control'");?>
      </div>


      <div class="form-group"><?php echo form_submit('submit', 'Change', "class='btn btn-default'");?></div>

<?php echo form_close();?>
</div>