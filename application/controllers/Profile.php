<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('books');
		$this->load->helper('url');
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			redirect('/', 'refresh');
		}
		$data['loggedin'] = $this->ion_auth->logged_in();
    $data["mybooks"] = $this->books->get_books_byuserid(1);
		$this->load->view('profile', $data);
	}
}
