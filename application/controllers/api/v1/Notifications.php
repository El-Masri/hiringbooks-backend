<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Notifications extends REST_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('notifications_model');
    $this->load->database();
  }

  public function index_get() {
    $user_id = $this->get('user_id');
    if ($user_id !== NULL) {
        if (!empty($this->notifications_model->get_byuserid($user_id))) {
            // Set the response and exit
            $this->response($this->notifications_model->get_byuserid($user_id), REST_Controller::HTTP_OK);
        }
        else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No notifications were found'
            ], REST_Controller::HTTP_NO_CONTENT); // NOT_FOUND (404) being the HTTP response code
        }
    } else {
      $this->response([
          'status' => FALSE,
          'message' => 'No notifications were found'
      ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
    }
  }

  public function index_post() {
    $data = $this->request->body;
    // if ($this->request->body !== NULL) {
    //   if ($this->messages_model->set_message($data)) {
    //       $this->response($data, REST_Controller::HTTP_OK);
    //   } else {
    //       $this->response([
    //           'status' => FALSE,
    //           'message' => 'Update has been failed'
    //       ], REST_Controller::HTTP_BAD_REQUEST);
    //   }
    // } else {
    //   $this->response([
    //       'status' => FALSE,
    //       'message' => 'No data were sent'
    //   ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
    // }
  }

  public function set_seen_post() {
    $data = $this->request->body;
    if ($this->request->body !== NULL) {
      if ($this->notifications_model->set_seen((Int) $data['id'])) {
          $this->response($data, REST_Controller::HTTP_OK);
      } else {
          $this->response([
              'status' => FALSE,
              'message' => 'Notification update has been failed'
          ], REST_Controller::HTTP_BAD_REQUEST);
      }
    } else {
      $this->response([
          'status' => FALSE,
          'message' => 'No data were sent'
      ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
    }
  }
}
