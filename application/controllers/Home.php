<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->model('books_model');
        $this->load->model('requests_model');
    }

    public function Index()
    {
        $data['loggedin'] = $this->ion_auth->logged_in();
        $books = $this->books_model->get_limited_books(4);
        $requests = $this->requests_model->get_limited_requests(4);
        $data['books'] = $books;
        $data['requests'] = $requests;
        $this->load->view('home', $data);
    }
}
